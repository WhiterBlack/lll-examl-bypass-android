/**
 * Created by whiterblack on 13.05.17.
 */
import React, { Component } from 'react';
import {
    AppRegistry,
    Text,
    View,
    TextInput,
    Alert,
    Linking
} from 'react-native';

const db = require('./db.json');
import lodash from 'lodash';

export default class app extends Component {
    constructor(props) {
        super(props);
        this.state = {
            searchstr:'',
            result:db
        }
    }
    render() {
        let out = [];
        for(let i = 0;i<5;i++) {
            if (!this.state.result[i]) break;
            out.push(
                <Text>Вопрос: {this.state.result[i].question}</Text>
            )
            this.state.result[i].true.forEach((el)=>{
                out.push(
                    <Text>Ответ: {el}</Text>
                )
            })
        }

        return (
            <View>
                <Text>Загружено {db.length} вопросов</Text>
                <Text>Под запрос подходит {this.state.result.length} Вопросов</Text>
                <TextInput
                    onChangeText={this.search.bind(this)}
                    value={this.state.searchstr}
                />
                {out}
                <Text onPress={()=>{
                    Alert.alert('О программе',
                        `Автор: Некто похожий на на Дару из Steins Gate

                        Лицензия данной софтины: Что похожее на BeerWare

                        Специально для прохождения теста от ЛЛЛ

                        Эль псай конгру...`
                    ,[
                            {text: 'Репозиторий', onPress: () => Linking.openURL('https://gitlab.com/WhiterBlack/lll-examl-bypass-android')}
                        ])
                }}>О программе</Text>
            </View>
        );
    }
    search(text) {
        this.setState({searchstr:text});
        let answer =  lodash.filter(db,(e)=>{
            try {
                return e.question.toLowerCase().includes(text.toLowerCase());
            } catch (_e) {
                console.error(e,_e);
            }

        })
        this.setState({result:answer});
    }
}


AppRegistry.registerComponent('app', () => app);